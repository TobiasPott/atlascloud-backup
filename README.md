# atlascloud-backup #

This is a tool to manage backups of your Jira & Confluence Atlassian Cloud instances. It downloads and manages backups of your cloud instances, provides monitoring using Opsgenie and helps with reporting and other monitoring integrations.

#### Features:

* **Clean:**
  * Easy configuration in YAML file
  * Support for multiple Confluence and Jira sites with distinct logins
  * Stores all backups in a tidy folder structure
  * Only keeps the n freshest backups - no disk overflow
  * Written as a really small Node.js CLI application - easy for you to validate, review and extend
* **Monitoring:**
  * Backup health assertion based on modification date and size relative to the previous backup - makes sure the most recent backup is fresh enough and did not shrink substantially compared to the previous one.
  * Easy to integrate with Nagios etc. - just run one CLI command and check the exit code.
  * Built-in Opsgenie integration featuring alerts and heartbeat - peace of mind that unless you get an alert, the status checks ran successfully.

## How to get up and running

### Initial setup

Let's install the tool and manually run the first backups!

#### Requirements

Before we begin, these are the requirements:

* A running node.js installation with npm.
* User and API key for the Atlassian cloud instances you want to backup (link to API key creation)
* (Optional) A configured Opsgenie REST API integration and a Opsgenie heartbeat.

#### Setup

Steps to go:

1. Clone the project repository
2. Copy `config.yml.dist` to `config.yml` and configure it to your liking:
   1. Add all the sites you want to back up
   2. Choose where to store the backups locally and how many backups to retain
3. Run `npm install` in the folder 

> **Pro-Tipp**: Backups with attachments are only allowed every 48 hours, but backups without attachments can run as often as you feel like. To set things up, it is recommended to run without attachments - then, switch them on when everything works.

#### First run

- Open a terminal in the project folder and type `node . --help` - a list of commands and parameters should appear. If not, you most likely have a problem in your Node setup or forgot to run `npm install`.
- Run `node . create` to create the first set of backups. The output should be something like:

```bash
Creating new Confluence backup for https://acme.atlassian.net
... requesting new backup creation.
... Backup Creation [========================================] 100% | ETA: 0s | Site export (backup) finished successfully.
... downloading finished backup to disk. Please stay patient.
... Backup Download [========================================] 100% | ETA: 0s | {info}
... success - backup saved locally.
Creating new Jira backup for https://acme.atlassian.net
... requesting new backup creation.
... started Jira backup creation.
... Backup Creation [========================================] 100% | ETA: 0s | Completed export
... downloading finished backup to disk. Please stay patient.
... Backup Download [========================================] 100% | ETA: 0s | {info}
... success - backup saved locally.
Managing backup retention for 2 sites.
... Done, removed 0 deprecated backups.
```

* Verify that the backup files are really there and really valid. 
* If it worked out, congratulations! Time to add cronjobs / planned tasks for your backups.

#### Troubleshooting

If problems happen, first try adding `-vv` as a parameter to the script - this will ramp up the debug output and maybe give a hint. If this does not help, dive into the code - we're happy to accept pull requests! ;)

### Planned Tasks

#### Creating backups

To periodically create and rotate backups, add a cronjob or planned task like

```bash
cd /path/to/atlascloud-backup && node . create
```

If the exit code is 0, the backup creation should have succeeded.

> **Please note:** backups with attachments are only possible every 48 hours. As Atlassian itself creates backups as well, you may opt to create full backups once every week or so.

#### Checking backup health

If you use a monitoring tool like Nagios or the like, you can easily integrate atlascloud-backup with it. Running

```bash
cd /path/to/atlascloud-backup && node . assert-status
```

checks the backups of all configured sites for two aspects:

* Is the newest backup for each site new enough? If the backup is older than `maxAgeHours` hours from the config file, this is considered a problem.
* Is the newest backup for each site approximately as big as the previous backup or bigger? If the backup is `maxSizeShrinkPercent`% smaller than the previous backup, this is considered a problem. Because usually, backups never become smaller. If your organisation is trigger-happy with space and project deletions, however, you might want to raise that threshold.

If any site violates one of those two tests, the exit code of `assert-status` ist 1, if everything seems fine it is 0.

> **Please note:** the tool only checks if backup files exist, their size and when they were modified. This does not guarantee that they are working backup files. You have to manually check if the backups are working.

#### Opsgenie integration

This tool features an Opsgenie integrationt that works as follows:

* The backup health is asserted just like with `assert-status` - but for each problem found, an Opsgenie alert is created via an Opsgenie REST integration.
* After the health is asserted and all alerts (hopefully none of course) are created, a heartbeat is sent.

_This way, you can rest assured that your backups work if no incident pops up - either because a problem was found or because the heartbeat was missing._

Heartbeats are optional - if you provide no heartbeat name, this step is skipped.

Just set the API key of a REST integration and the name of your heartbeat in the config file, add a cronjob and you are set.

```
cd /path/to/atlascloud-backup && node . ops-heartbeat
```

##### Opsgenie Integration Details

* The `source` of each alert is the hostname of the system atlascloud-backup runs on - so basically the return value of `os.hostname()`.
* An alias for the alerts is used so multiple alerts about the same backup are grouped by Opsgenie. The alias has the format `hostname/backuptype/subdomain`, so for example `backupnas/jira/acme`.

## Further reading

### Sources

This tool is based on the official example scripts for cloud backups from Atlassian - I wanted a little more monitoring and dislike bash.

* "[How to automate backups for Jira Cloud applications](https://confluence.atlassian.com/jirakb/how-to-automate-backups-for-jira-cloud-applications-779160659.html)"  in the Jira KB explains the basics
* [Atlassian Labs' automatic-cloud-backup](https://bitbucket.org/atlassianlabs/automatic-cloud-backup/src/master/) contains the official example scripts

### License

This tool is provided under the [ISC license](https://opensource.org/licenses/ISC).

I use this tool for our own, business-critical backups and so far it works. This does not mean it will work for you. Treat it with a certain amount of distrust.

### Support

This tool comes without support of any kind.

If you want to talk about it, I recommend heading over to the [Atlassian Community Forum](https://community.atlassian.com/).

### Who did this? ###

This tool is an act of love for the Atlassian community, made by [addcraft](https://addcraft.io/). We create Atlassian apps and sell them on the Atlassian Marketplace.

If you want to say thank you, consider trying one of our products - [have a look at our marketplace profile](https://marketplace.atlassian.com/vendors/1212423/).