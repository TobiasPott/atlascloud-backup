const axios = require("axios").default;
const fs = require("fs");
const backupStore = require('../backupstore');
var os = require("os");

let OPSGENIE_API_KEY = "";

async function createAlert (output, label, title, description, detailMap) {

    if (!OPSGENIE_API_KEY) {
        throw Error("Cannot create Opsgenie alert as no API key is set!");
    }


    output.debug("... preparing opsgenie api request.");

    const source = os.hostname();
    const alias = source+"/"+label;

    let res = null;
    try {
        res = await axios.post(
            "https://api.opsgenie.com/v2/alerts",
            {
                message: title,
                alias: alias,
                description: description,
                details: detailMap,
                source: source
            },
            {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "GenieKey "+OPSGENIE_API_KEY
                }
            }
        );
    }
    catch (ex) {
        console.log(ex);
        throw Error("Exception while sending Opsgenie request!")
    }

    if (!res || res.status != 202) {
        throw Error("Could not create Opsgenie alert, request returned status "+res.status+". Details: "+res.data);
    }

    output.info("... created Opsgenie alert via API. Opsgenie said '"+res.data.result+"', request ID is "+res.data.requestId+".");
    return true;
}

async function pingHeartbeat (output, name) {
    if (!OPSGENIE_API_KEY) {
        throw Error("Cannot send Opsgenie heartbeat as no API key is set!");
    }

    // Validate the name
    if (!name) {
        output.info("pingHeartbeat was skipped because no heartbeat name was supplied.");
        return false;
    }
    if (!name.match || !name.match(/^[a-z0-9\-_\.]+$/i)) {
        throw Error("Invalid heartbeat name '"+name+"' found. Please check configuration.");
    }

    output.debug("... preparing opsgenie api request.");

    let res = null;
    try {
        res = await axios.get(
            "https://api.opsgenie.com/v2/heartbeats/"+name+"/ping",
            {
                headers: {
                    "Authorization": "GenieKey "+OPSGENIE_API_KEY
                }
            }
        );
    }
    catch (ex) {
        console.log(ex);
        throw Error("Exception while sending Opsgenie request!");
    }

    if (!res || res.status != 202) {
        throw Error("Could not ping Opsgenie heartbeat, request returned status "+res.status+". Details: "+res.data);
    }

    output.info("... successfully pinged Opsgenie heartbeat '"+name+"', Opsgenie said '"+res.data.result+"', request ID is "+res.data.requestId+".");
    return true;
}

module.exports = {
    pingHeartbeat: pingHeartbeat,
    createAlert: createAlert,
    setApiKey: (v) => {
        OPSGENIE_API_KEY = v;
    }
};
