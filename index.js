#!/usr/bin/env node

const fs = require("fs");
const YAML = require("yaml");

const yargsObj = require("yargs");

const output = require("./src/output");
const atlassianAPI = require("./src/atlassian-api");
const backupStore = require('./src/backupstore');
const opsgenieAPI = require("./src/opsgenie-api");

let config = null;

function prepEnvironment (yargs) {
    output.setVerbose(yargs.argv.verbose);

    try {
        const file = fs.readFileSync(yargs.argv.config, 'utf8');
        config = YAML.parse(file);

        if (config.sites) {
            if (!config.sites.jira) config.sites.jira = {};
            if (!config.sites.confluence) config.sites.confluence = {};
        }
        else {
            config.sites = {confluence: {}, jira: {}};
        }

        if (config.backups) {
            if (config.backups.path) {
                if (config.backups.path.substr(-1) !== "/") {
                    config.backups.path += "/";
                }
                backupStore.setBaseDir(config.backups.path);
            }
                
            if (config.backups.retainCount)
                backupStore.setRetainCount(config.backups.retainCount);
            if (config.backups.maxAgeHours)
                backupStore.setMaxAgeHours(config.backups.maxAgeHours);
            if (config.backups.maxSizeShrinkPercent)
                backupStore.setMaxSizeShrinkPercent(config.backups.maxSizeShrinkPercent);

            if (config.backups.maxWaitMinutes)
                atlassianAPI.setMaxWaitMinutes(config.backups.maxWaitMinutes);
            if (config.backups.sleepSeconds)
                atlassianAPI.setSleepSeconds(config.backups.sleepSeconds);
        }

        if (config.opsgenie) {
            if (config.opsgenie.apiKey)
                opsgenieAPI.setApiKey(config.opsgenie.apiKey);
        }

    }
    catch (e) {
        var s = "Problem with reading config file '"+yargs.argv.config+"': "+e;
        output.err(s);
        throw Error(s);
    }
}

const options = yargsObj
    .usage("Usage: atlascloud-backup <command> [options]")

    .command("create", 'Creates new backups for every configured site and rotates.', async (yargs) => {
        prepEnvironment(yargs);

        try {
            for (var idx in config.sites.confluence) {
                var site = config.sites.confluence[idx];
                await atlassianAPI.createConfluenceBackup(output, idx, site.email, site.token, site.includeAttachments);
            }

            for (var idx2 in config.sites.jira) {
                var site2 = config.sites.jira[idx2];
                await atlassianAPI.createJiraBackup(output, idx2, site2.email, site2.token, site2.includeAttachments);
            }

            await backupStore.rotate(output, yargs.argv.dryrun);
        }
        catch (ex) {
            output.err(ex);
            process.exitCode = 1;
        }

    })

    .command("rotate", 'Deletes the oldest backups of each site that exceed the retainCount number of backups to keep. This is implicitly done when calling create, use this command for special rotation needs.', async (yargs) => {
        prepEnvironment(yargs);

        try {
            await backupStore.rotate(output, yargs.argv.dryrun);
        }
        catch (ex) {
            output.err(ex);
            process.exitCode = 1;
        }
    })

    .command("assert-status", "Asserts that the backups of all configured sites are new enough and not too small. Exit code signifies result: 0 is okay, 2 is problem.", async (yargs) => {
        prepEnvironment(yargs);

        // Collect sites from config
        let sites = [];
        for (var idx in config.sites.confluence) {
            sites.push({label: idx, type: "confluence"});
        }
        for (var idx2 in config.sites.jira) {
            sites.push({label: idx2, type: "jira"});
        }

        try {
            // Assert them all, but without opsgenie incident creation
            const problemCount = await backupStore.assertSites(output, sites, false);
            if (problemCount)
                process.exitCode = 2;
        }
        catch (ex) {
            output.err(ex);
            process.exitCode = 2;
        }
    })

    .command("ops-heartbeat", 'Asserts the status of all configured sites and creates an opsgenie incident in case of a problem. Sends an Opsgenie heartbeat after performing the check.', async (yargs) => {
        prepEnvironment(yargs);

        // Collect sites from config
        let sites = [];
        for (var idx in config.sites.confluence) {
            sites.push({label: idx, type: "confluence"});
        }
        for (var idx2 in config.sites.jira) {
            sites.push({label: idx2, type: "jira"});
        }

        try {
            // Assert them all, with opsgenie incident creation
            await backupStore.assertSites(output, sites, true);

            // If that has worked out, send the heartbeat!
            if (config.opsgenie && config.opsgenie.heartbeat) {
                await opsgenieAPI.pingHeartbeat(output, config.opsgenie.heartbeat);
                output.result("Sent heartbeat ping to Opsgenie.");
            }
        }
        catch (ex) {
            output.err(ex);
            process.exitCode = 1;
        }
    })

    .example("atlascloud-backup create --c /my/config.yml")
    .example("atlascloud-backup ops-heartbeat -vv")

    .option('config', {
        alias: 'c',
        describe: 'The YAML config file to use.',
        type: 'string',
        default: './config.yml',
        normalize: true
    })

    .option('verbose', {
        alias: 'v',
        describe: 'Raise verbosity of output. Double-v for double-noise like "-vv".',
        type: 'count'
    })

    .option('dryrun', {
        describe: 'If true, does not really delete files when rotating backups.',
        type: 'boolean',
        default: false
    })

    .demandCommand()
    .argv;